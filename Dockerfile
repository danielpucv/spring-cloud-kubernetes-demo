FROM openjdk:8u252

ADD target/spring-cloud-kubernetes-demo-0.0.1-SNAPSHOT.jar /opt/spring-cloud-kubernetes-demo-0.0.1-SNAPSHOT.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/opt/spring-cloud-kubernetes-demo-0.0.1-SNAPSHOT.jar"]